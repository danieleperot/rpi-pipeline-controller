import RPi.GPIO as GPIO

class Motor:
    def __init__(self, pin = 12, pwm = 100):
        self.pin = pin
        self.pwm = pwm
        self.init()
    
    def init(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin, GPIO.OUT)
        self.gpio = GPIO.PWM(self.pin, 50)
    
    def setPWM(self, pwm):
        self.pwm = pwm
    
    def start(self):
        self.gpio.start(self.pwm)

    def setPWMAndRestart(self, pwm):
        self.setPWM(pwm)
        self.start()
    
    def stop(self):
        self.gpio.stop()

