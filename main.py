#!/usr/bin/python3
import requests
from motor import Motor
from time import sleep

motor = Motor(12, 50)

print('#########################')
print('#  PIPELINE CONTROLLER  #')
print('#########################')

while True:
    print("\n\nCONTROLLO se devo avviare il motore...")
    r = requests.get('http://pipelinecontroller.truppaperot.it/')
    if r.text:
        print("RICHIESTA di avvio motore - PIPELINE: %s" % r.json()['pipeline'])
        print('\tPartenza motore')
        motor.start()
        sleep(7)
        print('\tArresto motore')
        motor.stop()
        print('NUOVO CONTROLLO in 15 secondi')
    else:
        print("NESSUNA RICHIESTA pendente. Nuovo tentativo in 15 secondi.")
    sleep(15)